.multilib-deb: &multilib-deb |
  multilib=$(dpkg-architecture -q DEB_TARGET_MULTIARCH)

.multilib-rpm: &multilib-rpm |
  multilib=$(rpm --eval '%{_lib}')

.environment: &environment |
  export NINJA="ninja"
  export VIRT_PREFIX="$HOME/build/libvirt"
  export PATH="$VIRT_PREFIX/bin:$HOME/.ccache/bin:$PATH"
  export C_INCLUDE_PATH="$VIRT_PREFIX/include"
  export LD_LIBRARY_PATH="$VIRT_PREFIX/$multilib"
  export PKG_CONFIG_PATH="$VIRT_PREFIX/$multilib/pkgconfig"
  export XDG_DATA_DIRS="$VIRT_PREFIX/share:/usr/share:/usr/local/share"
  export GI_TYPELIB_PATH="$VIRT_PREFIX/$multilib/girepository-1.0"
  export OSINFO_SYSTEM_DIR="$VIRT_PREFIX/share/osinfo"
  export MAKEFLAGS="-j $(getconf _NPROCESSORS_ONLN)"

.environment-centos-7: &environment-centos-7 |
  export NINJA="ninja-build"

.dco: &dco |
  pushd .
  ./scripts/require-dco.sh
  popd

.osinfo-db-tools-build: &osinfo-db-tools-build |
  pushd /tmp/
  git clone https://gitlab.com/libosinfo/osinfo-db-tools.git
  cd osinfo-db-tools
  mkdir build
  cd build
  meson .. . --prefix=$VIRT_PREFIX --werror
  $NINJA install
  popd

.osinfo-db-build: &osinfo-db-build |
  pushd /tmp/
  git clone https://gitlab.com/libosinfo/osinfo-db.git
  cd osinfo-db
  mkdir build
  cd build
  make -f ../Makefile VPATH=..
  make -f ../Makefile VPATH=.. check
  make -f ../Makefile VPATH=.. install OSINFO_DB_TARGET="--system"
  popd

.libosinfo-build: &libosinfo-build |
  pushd .
  mkdir build
  cd build
  meson .. . --prefix=$VIRT_PREFIX --werror
  $NINJA
  $NINJA install
  popd

.libosinfo-check: &libosinfo-check |
  pushd .
  cd build
  $NINJA test
  popd

.libosinfo-rpm: &libosinfo-rpm |
  pushd .
  cd build
  sed -i -e 's/BuildRequires: *osinfo-db.*//' *.spec*
  $NINJA dist
  rpmbuild --clean --define "_topdir `pwd`/rpmbuild" -ta meson-dist/*.tar.xz
  popd

.libosinfo-mingw32-build: &libosinfo-mingw32-build |
  export MINGW32_VIRT_PREFIX="$VIRT_PREFIX/i686-w64-mingw32/sys-root/mingw"
  export PKG_CONFIG_PATH="$MINGW32_VIRT_PREFIX/lib/pkgconfig"
  export PKG_CONFIG_LIBDIR="/usr/i686-w64-mingw32/sys-root/mingw/lib/pkgconfig:/usr/i686-w64-mingw32/sys-root/mingw/share/pkgconfig"
  pushd .
  mkdir build_win32
  cd build_win32
  meson .. . --cross-file=/usr/share/mingw/toolchain-mingw32.meson --prefix=$MINGW32_VIRT_PREFIX -Denable-gtk-doc=false -Denable-tests=false -Denable-introspection=disabled -Denable-vala=disabled --werror
  $NINJA
  $NINJA install
  popd

.libosinfo-mingw64-build: &libosinfo-mingw64-build |
  export MINGW64_VIRT_PREFIX="$VIRT_PREFIX/x86_64-w64-mingw32/sys-root/mingw"
  export PKG_CONFIG_PATH="$MINGW64_VIRT_PREFIX/lib/pkgconfig"
  export PKG_CONFIG_LIBDIR="/usr/x86_64-w64-mingw32/sys-root/mingw/lib/pkgconfig:/usr/x86_64-w64-mingw32/sys-root/mingw/share/pkgconfig"
  pushd .
  mkdir build_win64
  cd build_win64
  meson .. . --cross-file=/usr/share/mingw/toolchain-mingw64.meson --prefix=$MINGW64_VIRT_PREFIX -Denable-gtk-doc=false -Denable-tests=false -Denable-introspection=disabled -Denable-vala=disabled --werror
  $NINJA
  $NINJA install
  popd

centos-7:
  script:
    - *multilib-rpm
    - *environment
    - *environment-centos-7
    - *dco
    - *osinfo-db-tools-build
    - *libosinfo-build
    - *libosinfo-check
  image: quay.io/libvirt/buildenv-libosinfo-centos-7:latest

centos-8:
  script:
    - *multilib-rpm
    - *environment
    - *osinfo-db-tools-build
    - *libosinfo-build
    - *libosinfo-check
  image: quay.io/libvirt/buildenv-libosinfo-centos-8:latest

debian-9:
  script:
    - *multilib-deb
    - *environment
    - *dco
    - *osinfo-db-tools-build
    - *libosinfo-build
    - *libosinfo-check
  image: quay.io/libvirt/buildenv-libosinfo-debian-9:latest

debian-10:
  script:
    - *multilib-deb
    - *environment
    - *dco
    - *osinfo-db-tools-build
    - *osinfo-db-build
    - *libosinfo-build
    - *libosinfo-check
  image: quay.io/libvirt/buildenv-libosinfo-debian-10:latest

debian-sid:
  script:
    - *multilib-deb
    - *environment
    - *dco
    - *osinfo-db-tools-build
    - *osinfo-db-build
    - *libosinfo-build
    - *libosinfo-check
  image: quay.io/libvirt/buildenv-libosinfo-debian-sid:latest

fedora-30:
  script:
    - *multilib-rpm
    - *environment
    - *dco
    - *osinfo-db-tools-build
    - *osinfo-db-build
    - *libosinfo-build
    - *libosinfo-check
    - *libosinfo-rpm
    - *libosinfo-mingw32-build
    - *libosinfo-mingw64-build
  image: quay.io/libvirt/buildenv-libosinfo-fedora-30:latest

fedora-31:
  script:
    - *multilib-rpm
    - *environment
    - *dco
    - *osinfo-db-tools-build
    - *osinfo-db-build
    - *libosinfo-build
    - *libosinfo-check
    - *libosinfo-rpm
  image: quay.io/libvirt/buildenv-libosinfo-fedora-31:latest

fedora-rawhide:
  script:
    - *multilib-rpm
    - *environment
    - *dco
    - *osinfo-db-tools-build
    - *osinfo-db-build
    - *libosinfo-build
    - *libosinfo-check
    - *libosinfo-rpm
  image: quay.io/libvirt/buildenv-libosinfo-fedora-rawhide:latest

ubuntu-1604:
  script:
    - *multilib-deb
    - *environment
    - *dco
    - *osinfo-db-tools-build
    - *osinfo-db-build
    - *libosinfo-build
    - *libosinfo-check
  image: quay.io/libvirt/buildenv-libosinfo-ubuntu-1604:latest

ubuntu-1804:
  script:
    - *multilib-deb
    - *environment
    - *dco
    - *osinfo-db-tools-build
    - *osinfo-db-build
    - *libosinfo-build
    - *libosinfo-check
  image: quay.io/libvirt/buildenv-libosinfo-ubuntu-1804:latest
